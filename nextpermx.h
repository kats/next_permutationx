#include <algorithm>

// next permutation in reverse order
// == std::next_permutation(T::reverse_iterator begin, T::reverse_iterator end, greater<T>() )
template<typename Iter>
bool next_permutationX(Iter first, Iter last)
{
  if ( first == last ) return false;
  Iter i = first, j = i;
  for ( ; ++i != last && !(*j < *i); ++j ) {}
  if ( i != last ) { 
    for ( j = first; !(*j < *i); ++j ) {}
    std::iter_swap(i, j);
    std::reverse(first,i);
    return true;
  }
  std::reverse(first, last);
  return false;
}

