#include <iostream>
#include <vector>
#include <time.h>
#include "nextpermx.h"

using namespace std;

int main(int argc, char **argv) {
  time_t start,end;
  unsigned int n = 0;
  vector<short> vec;
  short lenv = 12;
  for ( short i = 0; i < lenv; ++i )
    vec.push_back(i);
  cout << "[ ";
  for ( short i = 0; i < lenv; ++i )
    cout << vec[i] << " ";
  cout << "] " << endl;

  // timing
  time (&start);
  do {
    ++n;
  } while (next_permutationX(vec.begin(),vec.end()));
  time (&end);
  cout << "[ ";
  for ( short i = 0; i < lenv; ++i )
    cout << vec[i] << " ";
  cout << "] " << n << " permutations, "<< difftime(end,start) << " sec" << endl;
  n = 0; 

  // timing
  time (&start);
  do {
    ++n;
  } while (next_permutation(vec.rbegin(),vec.rend(),greater<short>()) );
  time (&end);
  cout << "[ ";
  for ( short i = 0; i < lenv; ++i )
    cout << vec[i] << " ";
  cout << "] " << n << " permutations, "<< difftime(end,start) << " sec" << endl;

  // test permutations
  vector<short> vec1, vec2;
  lenv = 5;
  vec1.push_back(0);
  for ( short i = 0; i < lenv-1; ++i )
    vec1.push_back(i);
  vec2 = vec1;
  do {
//    cout << "[ ";
//    for ( short i = 0; i < lenv; ++i )
//      cout << vec1[i] << " ";
//    cout << "] ";
    cout << "[ ";
    for ( short i = 0; i < lenv; ++i )
      cout << vec2[i] << " ";
    cout << "]" << endl;
    if ( vec1 != vec2 )
      cout << "not equal!" << std::endl;
  } while (next_permutation(vec1.rbegin(),vec1.rend(),greater<short>()) && next_permutationX(vec2.begin(),vec2.end()));

  return 0;
}

